class ChangeDefaultValueForImageSrcOfBooks < ActiveRecord::Migration[6.1]
  def change
    change_column_default :books, :image_src, from: nil, to: 'placeholder_cover.jpg'
  end
end
