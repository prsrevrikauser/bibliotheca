Rails.application.routes.draw do
  root to: 'books#index'
  
  get 'all' => 'books#all', as: 'all_books'
  get 'search' => 'books#search'
  
  resources :books
end
